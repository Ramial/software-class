#include <iostream>

using namespace std;
/*
Description of the function

This is  the function main of our 
program, Class WS
Date: 28 November 2021
*/

int main(){
	
	//Declaration
	int myFirstVariable;
	float myFirstFloat;
	double myFirstDouble;
	char myFirstChar;
	
	//Initialize
	myFirstVariable = 1;
	myFirstFloat = 2.512345678;
	myFirstDouble = 2.512345678;
	myFirstChar = 'p';
	
	//Display of the variable
    cout<<myFirstVariable;

	
	return 0;
}
