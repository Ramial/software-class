#include <iostream>

using namespace std;

/*
Function example 03
*/
int main(){
	
	//Declaration of example
	int numberImput;
	int resultSum;
	int resultRest;
	int resultMult;
	int resultDiv;
	
	//Initialize
	numberImput = 0;
	resultSum = 0;
	resultRest = 0;
	resultMult = 0;
	resultDiv = 0;
	
	//Display phrase 1
	cout<<"Escribe un n�mero entero:";
	cin>>numberImput;
	
	//Operadores
	resultSum = numberImput + 2;
	resultRest = numberImput - 2;
	resultMult = numberImput * 2;
	resultDiv = numberImput / 2;
		
	//Display phrase 2 with number
	cout<<"el resultado de la suma es :"<<resultSum;
	cout<<"\nel resultado de la resta es :"<<resultRest;
	cout<<"\nel resultado de la multiplicaci�n es :"<<resultMult;
	cout<<"\nel resultado de la divisi�n es :"<<resultDiv;
	
	return 0;
}
