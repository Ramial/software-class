/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 3
 
 	3.- Ordene de mayor a menor 3 n�meros  ingresados por teclado
 ..
 * @version 1.0
 * @date 04.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int imput_number1, imput_number2, imput_number3;
int output_major, output_minor, output_betweennumber;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
bool Collectdata();
void Ordering_numbers_major_to_minor(int number1, int number2, int number3);
void Results();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Collectdata(){
	cout<<"\nEl siguiente programa ordenar� de menor a mayor el programa"<<endl;
	cout<<"=========================INSERT DATA ===============================\n";
	cout<<"Ingresa 3 n�meros a ordenar: ";
	cin>> imput_number1 >> imput_number2 >> imput_number3;
	
	if (cin.fail()){
		return true;
	}
	
		if(imput_number1 == imput_number2 || imput_number2 == imput_number3 || imput_number3 == imput_number1){
		return false;
		}
	return true;
}
//=====================================================================================================

void Ordering_numbers_major_to_minor(int number1, int number2, int number3){
	
	if((number1 > number2) && (number1 > number3)){
		output_major = number1;
		if(number2 > number3){
			output_betweennumber = number2;
			output_minor = number3;
		} else {
			output_betweennumber = number3;
			output_minor = number2;
		}
	}
	
	if((number2 > number1) && (number2 > number3)){
		output_major = number2;
		if(number1 > number3){
			output_betweennumber = number1 ;
		 	output_minor = number3;
		} else {
			output_betweennumber = number3 ;
			output_minor = number1  ;
		}
	}
		
	if((number3 > number2) && (number3 > number1)){
		output_major = number3  ;
		if(number2 > number1){
			output_betweennumber = number2  ;
			output_minor = number1  ;
		} else {
			output_betweennumber = number1  ;
			output_minor = number2  ;
		}
	}
	
}

//=====================================================================================================

void Results(){
	if (cin.fail()){
		return;
	}
	cout<<"==================================RESULTS====================================\n";
	cout<<"El orden de los n�meros introducidos es: "<< output_major <<"	"<< output_betweennumber <<"	"<< output_minor;
}

void Run(){
	while (true){
		
		if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"\n=========================ERROR==========================\n";
		cout<<"Incorret type of data";
		continue;
		}
		
		if (Collectdata() == false){
		cout<<"\n================ERROR======================\n";
		cout<<"Error giving information, do it again\n";
		
			if(imput_number1 == imput_number2 || imput_number2 == imput_number3 || imput_number3 == imput_number1){
			cout<<"Please, introduce different numbers\n";
			}
		
		continue;
		}
	
	Ordering_numbers_major_to_minor(imput_number1, imput_number2, imput_number3);
	Results();
		
	}
	return;
}

