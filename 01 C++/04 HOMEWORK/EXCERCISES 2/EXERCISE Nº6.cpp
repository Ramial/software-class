/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N�
		6.- Dada la hora del d�a en horas, minutos y segundos encuentre la hora del siguiente segundo.
 ..
 * @version 1.0
 * @date 04.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
char notgonnause;
int global_imput_hours;
int global_imput_minutes;
int global_imput_seconds;
int global_output_hours;
int global_output_seconds;
int global_output_minutes;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
int Check_new_time(int hours, int minutes, int seconds);
void Results();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"Se hallar� la hora del siguiente segundo"<<endl;
	cout<<"=======================================INSERT DATA================================================\n";
	cout<<"Ingrese la hora (HH : MM : SS) a convertir: ";
	cin>> global_imput_hours >> notgonnause >> global_imput_minutes >>notgonnause >> global_imput_seconds;
}
//=====================================================================================================

void Run(){
	Collectdata();
	
	if(cin.fail()){
		cin.clear();
		cout<<"\n=========================ERROR=============================\n";
		cout<<"Incorrect type of data";
		return;
	}
	
	if(global_imput_hours >= 24 || global_imput_minutes >= 60 || global_imput_seconds >= 60){
	
	cout<<"\n=========================ERROR=============================\n";
	
	if(global_imput_hours >= 24){
		cout<<"The hour cannot be more than 23"<<endl;
	}
	
	if(global_imput_minutes >= 60){
		cout<<"The minutes cannot be more than 59"<<endl;
	}
	
	if(global_imput_seconds >= 60){
		cout<<"The seconds cannot be more than 59"<<endl;
	}
	return;
}
	Check_new_time(global_imput_hours, global_imput_minutes, global_imput_seconds);
	Results();
}
//=====================================================================================================

int Check_new_time(int hours, int minutes, int seconds){
	
	seconds = seconds + 1;
		
	if (seconds==60){
		seconds = 0;
		minutes = minutes + 1;
	}
	
	if (minutes == 60){
		minutes = 0;
		hours = hours + 1;
	}
	
	if (hours == 24){
		hours = 0;
	}
	
	global_output_seconds = seconds;
	global_output_minutes = minutes;
	global_output_hours = hours;
}

//=====================================================================================================

void Results(){
	
	cout<<"=================================RESULTS=============================\n";
	cout<<"La hora un segundo despu�s es: "<< global_output_hours << ":" << global_output_minutes << ":" << global_output_seconds;
	
}

