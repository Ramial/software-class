/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIO N� 4
4.- Dado un tiempo en minutos, calcular los d�as, horas y minutos que    le corresponden.
 ..
 * @version 1.0
 * @date 04.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int global_imput_minutes;
int global_output_days;
int global_output_hours;
int global_output_minutes;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
bool Collectdata();
void Check_amount_of_minutes();
void Results();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Collectdata(){
	cout<<"\nEl siguiente programa convertir� los minutos introducidos en d�as, horas y minutos correspondientes\n";
	cout<<"=======================================INSERT DATA======================================================\n";
	cout<<"Ingres la cantidad de minutos a convertir: ";
	cin>> global_imput_minutes;
	cout<<endl;
	if (cin.fail()){
		return true;
	}
	if (global_imput_minutes <= 0){
		return false;
	}
	return true;
}
//=====================================================================================================

void Check_amount_of_minutes(int minutes){
	if(minutes > 1440){
		global_output_days = minutes / 1440;
		minutes = minutes % 1440;
	} if (minutes > 60){
		global_output_hours = minutes / 60;
		minutes = minutes % 60;
	} 
		global_output_minutes = minutes;
	}	

//=====================================================================================================

void Results(){
	
	if (cin.fail()){
		return;
	}
	cout<<"====================================RESULTS========================================\n";
	cout<<"El resultado es: "<< global_output_days << " d�as, "<< global_output_hours << " horas, "<< global_output_minutes << " minutos";
	
}

void Run(){
	while (true){

	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n'); 
		cout<<"\n==========================ERROR=========================="<<endl;
		cout<<"Incorrect type of data";
		continue;
	}

	if (Collectdata() == false){
		cout<<"\n==========================ERROR=========================="<<endl;	
		cout<<"Please introduce a positive number\n";
		continue;
	}
	
	
	
	Check_amount_of_minutes(global_imput_minutes);
	Results();
 	}
 	return;
}
