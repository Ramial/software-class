/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N�
		
		12.- Calcular la utilidad que un trabajador recibe en el reparto anual de utilidades 
		si este se le asigna como un porcentaje de su salario mensual que depende de su antig�edad 
		en la empresa de acuerdo con la sig. tabla:
		Tiempo						Utilidad
	Menos de 1 a�o								5 % del salario
	1 a�o o mas y menos de 2 a�os				7% del salario
	2 a�os o mas y menos de 5 a�os				10% del salario
	5 a�os o mas y menos de 10 a�os				15% del salario
	10 a�os o mas								20% del salario

 
 * @version 2.0
 * @date 12	.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int global_imput_years_in_corporation;
float global_imput_salary;
float global_output_utility;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
void Results();
float check_utility_porcentage(int years);
float Check_amount_of_utility(float salary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
		cout<<"Este programa programa te asignar� un porcentaje de utilidad y la utilidad\n";
		cout<<"================================INSERT DATA================================\n";
		cout<<"Ingrese la cantidad de a�os en la empresa: "; cin>>global_imput_years_in_corporation;
		cout<<"Ingrese el salario que percibe: "; cin>>global_imput_salary;
}
//=====================================================================================================

void Run(){
	Collectdata();
	Check_amount_of_utility(global_imput_salary);
	Results();
}
//=====================================================================================================

float check_utility_porcentage(int years){
	float utility_porcentage;
	
	if(years < 1){
		utility_porcentage = 0.05;
	} else if(years < 2) { 
		utility_porcentage = 0.07;	
	} else if(years < 5){
		utility_porcentage = 0.1;
	} else if (years < 10){
		utility_porcentage = 0.15;
	} else if (years >= 10){
		utility_porcentage = 0.2;
	}
	
	return utility_porcentage;
}
//=====================================================================================================

float Check_amount_of_utility(float salary){
	global_output_utility = salary * check_utility_porcentage(global_imput_years_in_corporation);
	
}

//=====================================================================================================

void Results(){
	cout<<"===================================RESULTS=====================================\n";
	cout<<"Le corresponde una utilidad de: "<< global_output_utility <<" soles";
}

