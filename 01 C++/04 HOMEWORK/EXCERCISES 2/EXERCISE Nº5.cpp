/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 5
	.- Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo,
	 determine si los datos corresponden a un triangulo. En caso afirmativo, escriba si el triangulo 
	 es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea.
 ..
 * @version 1.0
 * @date 04.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float global_imput_lado1, global_imput_lado2, global_imput_lado3;
bool existance_of_triangule;
double global_output_areatriangule;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Results();
void Collectdata();
void Check_existace_of_triangule(int lado1, int lado2, int lado3);
float Area_of_the_triangule(float lado1, float lado2, float lado3);
void Type_of_triangule(int lado1, int lado2, int lado3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"El siguiente programa comprobar� la existencia de un triangulo, tipo de triangulo y su �rea\n";
	cout<<"================================INSERT DATA========================================\n";
	cout<<"Ingrese los lados a evaluar: ";
	cin>> global_imput_lado1 >> global_imput_lado2 >> global_imput_lado3;
	

}
//=====================================================================================================

void Check_existace_of_triangule(int lado1, int lado2, int lado3){
	if (lado1 > lado2){
		if((lado1+lado2 > lado3) && (lado1-lado2 < lado3)) {
			existance_of_triangule = true;
		} else {
			existance_of_triangule = false;
		}
	} else if (lado2 > lado1){
		if((lado1 + lado2 > lado3) && (lado2 - lado1 < lado3)) {
			existance_of_triangule = true;
		} else {
			existance_of_triangule = false;
		}
	}
}

//=====================================================================================================

void Type_of_triangule(int lado1, int lado2, int lado3){
	if (existance_of_triangule = true){
		if (lado1 == lado2  && lado1 == lado3 && lado2 == lado3){
			cout<<"El tri�ngulo es equilatero\n";
		} else if ((lado1 == lado2) || (lado2 == lado3) || (lado1 == lado3)){
			cout<<"El tri�ngulo es is�celes\n";
		} else {
			cout<<"El tri�ngulo es escaleno\n";
		}
	} 
	
}

//=====================================================================================================

float Area_of_the_triangule(float lado1, float lado2, float lado3){
	// Variables locales
	float perimetro_of_triangule;
	float semiperimetro_of_triangule;
	// operations
	perimetro_of_triangule = lado1 + lado2 + lado3;
	semiperimetro_of_triangule = perimetro_of_triangule / 2;
	
	if (existance_of_triangule = true){
		global_output_areatriangule = sqrt((semiperimetro_of_triangule-lado1)+(semiperimetro_of_triangule-lado2)+(semiperimetro_of_triangule-lado3)*(semiperimetro_of_triangule));
	}
}

//=====================================================================================================

void Results(){
	cout<<"=================================RESULTS======================================\n";
	if(existance_of_triangule=true){
		cout<<"El tri�ngulo existe\n";
		Type_of_triangule(global_imput_lado1, global_imput_lado2, global_imput_lado3);
		cout<<"El area del tri�nglo es: "<< global_output_areatriangule<<endl;
	} else{
		cout<< "EL TRIANGULO NO EXISTE";
	}
}

//=====================================================================================================
void Run(){
	Collectdata();
	
	if (cin.fail()){
		cin.clear();
		cout<<"\n==============================ERROR=====================================\n";
		cout<<"Incorrect type of data";
		return;
	}
	
	Check_existace_of_triangule(global_imput_lado1, global_imput_lado2, global_imput_lado3);
	Area_of_the_triangule(global_imput_lado1, global_imput_lado2, global_imput_lado3);
	Results();	
		
	
}
//=====================================================================================================

