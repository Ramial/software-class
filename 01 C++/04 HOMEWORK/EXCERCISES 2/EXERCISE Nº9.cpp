/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N�
		
		Calcular la comisi�n sobre las ventas totales de un empleado, sabiendo que el empleado 
		no recibe comisi�n si su venta es hasta S/.150, si la venta es superior a S/.150 y menor 
		o igual a S/.400 el empleado recibe una comisi�n del 10% de las ventas y si las ventas 
		son mayores a 400, entonces la comisi�n es de S/.50 m�s el 9% de las ventas.
 
 * @version 1.0
 * @date 11.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float global_imput_sells;
float global_output_comission;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
void Results();
float Check_comission(float sells, float comision);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"El siguiente programa hallar� la comisi�n que te corresponde de acuerdo a las ventas generadas\n";
	cout<<"===================================INSERT DATA=====================================\n";
	cout<<"Ingrese las ventas totales generadas: "; cin>> global_imput_sells;
	
}
//=====================================================================================================

void Run(){
	bool check =  true;
	
	while (check = true){
	
		Collectdata();
		if ( cin.fail() ){
			cin.clear();
			cin.ignore(1000,'\n');
			cout<<"\n==============================ERROR==============================\n";
			cout<<"Incorrect type of data\n\n";
			continue;
		}
		check = false;
		Results();
	}
}
//=====================================================================================================

float Check_comission(float sells, float comision){
	if (sells <= 150){
		comision = 0.0;
	} else if (sells > 150 && sells <= 400){
		comision = 0.1 * sells;
	} else if (sells > 400){
		comision = 50.0 + (0.09 * sells);
	}
	
	return comision;
}

//=====================================================================================================

void Results(){
	cout<<"=======================RESULTS========================\n";
	cout<<"La comisi�n que le correponde es de: "<< Check_comission(global_imput_sells, global_output_comission)<<" soles.";
}

