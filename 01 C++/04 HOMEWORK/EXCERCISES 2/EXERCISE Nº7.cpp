/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 7 
	..
	Una compa��a de alquiler de autos emite la factura de sus clientes teniendo en cuenta la distancia recorrida, 
	si la distancia no rebasa los 300 km., se cobra una tarifa fija de S/.250, si la distancia recorrida es mayor a 300 km. 
	y hasta 1000 km. Se cobra la tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km. y si la distancia recorrida 
	es mayor a 1000 km.,  la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 1000 a raz�n de S/. 30, y S/.20
	 para las distancias mayores de 1000 km. Calcular el monto que pagar� un cliente.
 
 * @version 1.0
 * @date 11.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int global_imput_distance;
int global_output_payment;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
int Check_amount_payment(int distance);
void Results();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"Se emitir� la factura a pagar en base al kilometraje"<<endl;
	cout<<"===============================INSERT DATA ===================================\n";
	cout<<"Ingrese la distancia recorrida: "; cin>> global_imput_distance;
}
//=====================================================================================================

void Run(){
	
	bool check = true;
	
	while(check == true){
	Collectdata();
	
	if(cin.fail()){
	cin.clear();
	cin.ignore(1000,'\n');
	cout<<endl<<"=========================ERROR==========================\n";
	cout<<"Incorrect type of data\n"<<endl;
	continue ;
	}
	Check_amount_payment(global_imput_distance);
	Results();
	check = false;
	
}
}
//=====================================================================================================

int Check_amount_payment(int distance){
	if (distance <= 300){
		global_output_payment = 250;
	} else if ( distance <= 1000){
		distance = distance - 300;
		global_output_payment = 250 + (distance * 30);
	} else if ( distance > 1000){
		distance = distance - 1000;
		global_output_payment = 250 + (30 * 700) + (20 * distance);
	}
}

//=====================================================================================================

void Results(){
	
	cout<<"==============================RESULTS================================\n";
	cout<<"El monto a pagar es: "<< global_output_payment << " dolares";
}

