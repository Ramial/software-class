/**
 * @file EXERCISES N� 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIO N� 2
 	A un  trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000, 
	 por encima de 1000 hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del adicional. 
	 Calcular el descuento y sueldo neto que recibe el trabajador dado un sueldo,
 * @version 1.0
 * @date 04.02.2022
 * 
 */

..
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 ***********************************	********************************************************************************************************/
float global_imput_payment = 0.0;
float global_discount_payment = 0.0;
float global_output_payment = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
bool Collectdata();
float check_discount_of_payment(int payment);
float new_amount_of_payment(float payment);
bool Results();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Collectdata(){
	cout<<"=================================INSERT DATA===================================="<<endl;
	cout<<"Ingrese la cantidad de sueldo que le corresponde: ";
	cin>>global_imput_payment;
	
	if(global_imput_payment < 0 ){
		cout<<"Error negative number is not valid\n";
		return false;
	}
	if(global_imput_payment = 0 ){
		cout<<"Error nule number is not valid\n";
		return false;
		}
	if (cin.fail()){
	return false;
	}
	
}
//=====================================================================================================

float check_discount_of_payment(float payment){
	if (payment<=1000){
			global_discount_payment = 10;
	}	else if (payment<=2000){
			global_discount_payment = 5;
	}	else { 
			global_discount_payment = 3;
		}
	}

//=====================================================================================================

float new_amount_of_payment(float payment){
    global_discount_payment = 100 - global_discount_payment;
	global_output_payment = global_imput_payment * global_discount_payment *0.01;
}

//=====================================================================================================

bool Results(){
	
	if(global_output_payment < 0){
		cout<<"\n Error in results, negative number\n";
		return false;
	}
	cout<<"===================================RESULTS========================================="<<endl;
	cout<<"El pago que debe recibir con todo descuento es: "<< global_output_payment<<endl;
	return true;
}

//=====================================================================================================

void Run(){
	
	while(true){
	
		if (Collectdata() == false){
			cout<<"\n==============================ERROR=========================================\n";
				if(global_imput_payment < 0 ){
					cout<<"Error negative number is not valid\n";
				}
				
				if(global_imput_payment = 0 ){
					cout<<"Error nule number is not valid\n";
				}
				
				if (cin.fail()){
					cin.clear();
					cin.ignore(1000,'\n');
					cout<<"Incorrect type of data\n";
				}
			continue;	
		}
	
		check_discount_of_payment(global_imput_payment);
		new_amount_of_payment( global_imput_payment );
		if (Results() == false){
			continue;
		}
		
		break;
	}
	return;
}
