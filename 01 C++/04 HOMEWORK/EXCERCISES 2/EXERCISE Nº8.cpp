/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N�8
 
		Una empresa registra el sexo, edad y estado civil de sus empleados a trav�s de un n�mero entero 
		positivo de cuatro cifras de acuerdo a lo siguiente: la primera cifra de la izquierda representa 
		el estado civil (1 para soltero, 2 para casado, 3 para viudo y 4 para divorciado), las siguientes 
		dos cifras representan la edad y la tercera cifra representa el sexo (1 para femenino y 2 para masculino). 
		Dise�e un programa que determine el estado civil, edad y sexo de un empleado conociendo el n�mero que empaqueta 
		dicha informaci�n.
 
 * @version 1.0
 * @date 11.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int global_imput_number;
int age_code = 1; 
int estado_civil_code = 1;
int sex_code = 1;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
void Results();
void Check_information_of_the_number(int estadocivil, int sex);
void Separating_numbers(int num);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"El siguiente programa detectar� edad, estado civil y sexo de acuerdo a un c�digo\n";
	cout<<"=========================================INSERT DATA====================================\n";
	cout<<"Ingrese el n�mero de 4 cifras: "; cin>> global_imput_number;
}

//=====================================================================================================

void Separating_numbers(int num){
	
	estado_civil_code = num / 1000;
	sex_code = num % 10;
	age_code = (num % 1000 - sex_code)/10;
	
}



//=====================================================================================================

void Results(){
	cout<<"===============================RESULTS======================================\n";
	cout<<"La edad de la persona es: "<< age_code <<endl;
}

//=====================================================================================================

void Check_information_of_the_number(int estadocivil, int sex){
	cout<<"Estado civil: ";
	if (estadocivil == 1){
		cout<< "ES SOLTERO(A)\n";
	} else if (estadocivil ==2){
		cout<< "ES CASADO(A)\n";
	} else if (estadocivil == 3){
		cout<< "ES VIUDO(A)\n";
	} else if (estadocivil ==4){
		cout<< "ES DIVORCIADO(A)\n";
	}
	cout<<"Su sexo es: ";
	if (sex == 1){
		cout<< " MUJER\n";
	} else if ( sex == 2){
		cout<< " HOMBRE\n";
	}
	
}
//=====================================================================================================

void Run(){
	
	bool check = true;
	
	while (check = true){
	
	Collectdata();
	
	if(cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"\n=============ERROR==================\n";
		cout<<"Incorrect type of data\n"<<endl;
		continue;
	}
	Separating_numbers(global_imput_number);
	
	if (estado_civil_code < 5 && estado_civil_code > 0){
		if (sex_code == 1 || sex_code ==2){
				Results();
				Check_information_of_the_number(estado_civil_code, sex_code);		
		}
	} else {
		cout<<"C�DIGO INV�LIDO"<<endl<<endl;
		continue;
	}
	check = false;
}
}
