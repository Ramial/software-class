/**
 * @file EJERCICIOS N�2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIO N�1 
 		Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
		Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que 
		una hora normal.
		Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.
..
 * @version 1.0
 * @date 29.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int global_imput_hoursworked = 0;			// HORAS QUE SE TRABAJA SEMANALMENTE
int global_imput_payperhour = 0;			// PAGO POR HORAS
float payment = 0;							// PAGO INICIAL SIN DESCUENTO
float global_output_payment = 0;			// PAGO FINAL CON EL DESCUENTO SI CUMPLE CONDICIONES

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
float Final_check_payment();
bool Collectdata();
float Checkhoursworked (float hoursworked);
float initialamountofpayment (float hoursworked, float payperhour);
float finalamountofpayment (float initialpayment);


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
		Final_check_payment();
		
	return 0;
}


/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Collectdata(){
	cout<<"=========================INSERT DATA===========================\n";
	cout<<"ESTE PROGRAMA CALCULAR� EL PAGO SEMANAL DE UN TRABAJADOR"<<endl<<"Para las horas mayores a 40 se cobrar� 50% m�s\n";
	cout<<"Para los sueldos mayores a 500 se le har� un descuento del 10%"<<endl;
	cout<<"Ingrese el total de horas trabajadas durante la semana: ";
	cin>>global_imput_hoursworked;
	cout<<"Ingrese el pago que se le realizar� por hora: ";
	cin>>global_imput_payperhour;
	
	if (global_imput_hoursworked < 0){
		cout<<"\nThe hours worked cannot be negative, not valid\n";
		return false;
	}
	
	if (global_imput_payperhour < 0){
		cout<<"The payment cannot be negative, not valid/n";
		return false;
	}
	
		if (cin.fail()){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"\n==============================ERROR=========================================\n";
		cout<<"Incorrect type of data\n";
		return false;
	}
	
		return true;	
}
//=====================================================================================================

float Checkhoursworked(float hoursworked){
	if (hoursworked >=160){
		cout<<endl<<"============================= NOTE================================="<<endl;
		cout<<"�La siguiente que mientas con las horas no te pago!"<<endl;
		cout<<endl;
				hoursworked = 0;
		
	}	else if (hoursworked >= 126) {
		cout<<endl<<"============================= NOTE================================="<<endl;
		cout<<"Como que necesitas dormir un poco m�s"<<endl;
		cout<<endl;

		
	} else if (hoursworked>40){
		float extrahours;
			extrahours = global_imput_hoursworked - 40;
			hoursworked = global_imput_hoursworked + extrahours/2;	
			
			cout<<hoursworked<<endl;
		}
	return hoursworked;
}

//=====================================================================================================	

float initialamountofpayment (float hoursworked, float payperhour){
	payment = hoursworked * payperhour;
	return payment;	
}

//=====================================================================================================

float finalamountofpayment (float initialpayment){
	
	if (initialpayment>500){
		global_output_payment = initialpayment * 9 / 10;
	}
	return global_output_payment;
}

float Final_check_payment(){
	
	while (true){
		
	if (Collectdata() == false){
		cout<<"Error in Collectdata, type the code again\n"<<endl;
		continue;
	}	
	break;
	}
		
	finalamountofpayment(initialamountofpayment(Checkhoursworked(global_imput_hoursworked),global_imput_payperhour)) ;
	
	cout<<"======================RESULTS========================"<<endl;
	cout<<"La cantidad a pagar al trabajador es: ";
	cout<<"$ "<< global_output_payment <<endl;
	return 0;
}

