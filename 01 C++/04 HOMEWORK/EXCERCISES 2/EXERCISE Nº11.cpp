/**
 * @file EJERCICIOS 2
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 11
 
 	Se necesita un sistema para un supermercado, en el cual si el monto de la compra del cliente es mayor 
	 de $5000 se le har� un descuento del 30%, si es menor o igual a $5000 pero mayor que $3000 ser� del 20%, 
	 si no rebasa los $3000 pero si los $1000 la rebaja efectiva es del 10% y en caso de que no rebase los $1000 
	 no tendr� beneficio.

 
 * @version 1.0
 * @date 12.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float global_imput_monto;
float global_output_discount;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Collectdata();
void Results();
float Confirm_amount_of_discount(float monto, float discount);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"El siguiente programa te dir� el porcentaje de descuento que te corresponde de acuerdo al monto a pagar\n";
	cout<<"==================================INSERT DATA=========================================\n";
	cout<<"Ingrese el monto a pagar: "; cin>> global_imput_monto;
}
//=====================================================================================================

void Run(){
	Collectdata();
	if (cin.fail()){
		cin.clear();
		cout<<"\n=================ERROR=================\n";
		cout<<"Incorrect type of data";
		return;
	}
	Results();
}
//=====================================================================================================

float Confirm_amount_of_discount(float monto, float discount){
	if(monto <= 1000){
		discount = 0;
	} else if (monto <=3000){
		discount = 10;
	} else if (monto <=5000){
		discount = 20;
	} else if (monto > 5000){
		discount = 30;
	}
	return discount;
}

//=====================================================================================================

void Results(){
	cout<<"==============================RESULTS================================\n";
	cout<<"El descuento que le corresponde es de "<< Confirm_amount_of_discount(global_imput_monto, global_output_discount)<<"%";
	
}

