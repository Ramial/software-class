/**
 * @file EJERCICIOS N� 18
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 18
		Si coloca una escalera de 3 metros a un �ngulo de 85 grados al lado de un edificio, 
		la altura en la cual la escalera toca el edificio se puede calcular como altura=3 * seno 85�. 
		Calcule esta altura con una calculadora y luego escriba un programa en C que obtenga y visualice el valor de la altura. 
Nota: Los argumentos de todas las funciones trigonom�tricas (seno, coseno, etc) deben estar expresados en radianes.
 Por tanto, para obtener el seno, por ejemplo, de un �ngulo expresado en grados, primero deber� convertir el �ngulo a radianes.


 * @version 1.0
 * @date 21.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416
/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 float angulo, largo;			// variable para operaciones
 float seno, altura;   			//  variables de resultados
 

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"ESTE PROGRAMA HALLAR� LA ATURA A LA QUE UNA ESCALERA DESCANSA SOBRE UN EDIFICIO"<<endl;
	cout<<"INGRESE EL LARGO DE LA ESCALERA(en metros): "; cin>>largo;
	cout<<"INGRESE EL ANGULO QUE FORMA CON LA HORIZONTAL: "; cin>>angulo;
    }
	
//=====================================================================================================

void Operations() {
	seno = sin(angulo*PI/180);
	altura = seno * largo;

}
//=====================================================================================================

void Results(){
	cout<<"LA ALTURA A LA QUE UNA ESCALERA DESCANSA SOBRE UN EDIFICIO ES: "<<endl;
	cout<< altura <<" metros"<<endl;
	
}
//=====================================================================================================

void Run(){
	Collectdata();
	Operations();
	Results();
	
	}
