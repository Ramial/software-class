/*
HOMEWORK EXERCISE N�6

 * @Ejercicio N�6.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 06:
 		Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.
 * @version 1.0
 * @date 12.01.2022
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int time=0, segundos, minutos, horas, dias;   // variables para las operaciones
int d = 24*60*60;								// 	segundos en un d�a
int h = 60*60;									//   segundos en una hora
int m = 60;										// 	segundos en un minuto

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();				// Funci�n que contendr� a las dem�s funciones
void Collectdata();		// Funci�n que recoger� los datos ingresados
void Operations();		// Funci�n que realizar� las operaciones matem�ticas
void Results();			// Funci�n que imprimir� los resultados

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"========================INSERT DATA================================"<<endl;
	cout<<"Convertidor de segundos a minutos, horas y d�as"<<endl;
	cout<<"INGRESE LOS SEGUNDOS QUE QUIERE CONVERTIR: ";					// insertar el n�mero de segundos a convertir
	cin>>time;																// igualar el n�mero insertado a la variable
	
}
//=====================================================================================================

void Operations(){
	dias = time/d;		//Cantidad de d�as
	time %= d;			//residuo de la divisi�n anterior
	horas= time/h; 		//Cantidad de horas	
	time %=h;			//residuo de la divisi�n anterior
	minutos=time/m;		//Cantidad de minutos 
	time %= m;			//residuo de la division anterior
	segundos=time;
	
}
//=====================================================================================================
void Results(){
	cout<<"El tiempo convertido es: "<< dias<<" d�as, "<<horas<<" horas, "<< minutos<<" minutos con "<< segundos<<" segundos";
	
}

//=====================================================================================================
void Run(){
	Collectdata();
	Operations();
	Results();
}
//=====================================================================================================

