/**
 * @file EJERCICIOS N� 13
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 13
 Un tonel es un recipiente, generalmente de madera, muy utilizado para almacenar y mejorar un vino. 
 La forma de un tonel es muy caracter�stica y es un cilindro en el que la parte central es m�s gruesa, es decir, 
 tiene un di�metro mayor que los extremos. Escriba un programa que lea las medidas de un tonel y nos devuelva su
capacidad, teniendo en cuenta que el volumen (V) de un tonel viene dado por la siguiente f�rmula: V = ? l a2 donde:

 l    es la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)

d   es el di�metro del tonel en sus extremos.

D  es el di�metro del tonel en el centro: D>d

Nota: Observe que si las medidas se dan en cent�metros el resultado lo obtenemos en cent�metros c�bicos.

 
 * @version 1.0
 * @date 19.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define pi  3.1416;

/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float dcentro, dextrem, altura, largo, volumen;  // variables a usar en el problema

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"ESTE PROGRAMA HALLAR� EL VOLUMEN DE UN TONEL"<<endl;
	cout<<"INGRESE LA LONGITUD DEL TONEL EN CENTIMETROS: "<<endl;
	cin>>largo;
	cout<<"INGRESE EL DIAMETRO EN EL CENTRO DEL TONEL EN CENTIMETROS"<<endl;
	cin>>dcentro;
	cout<<"INGRESE EL DIAMETRO EN LOS EXTREMOS DEL TONEL EN CENTIMETROS"<<endl;
	cin>>dextrem;
}
//=====================================================================================================

void Operations(){
		
	altura = (dextrem/2) + (2/3)*(dcentro/2)+(2*3)*(dextrem/2);
	volumen = 3.1416 * largo * pow (altura,2);

}
//=====================================================================================================

void Results(){
	cout<<"EL VOLUMEN DE DICHO TONEL ES: "<< volumen << " CENTIMETROS C�BICOS"<<endl;
	
}
//=====================================================================================================

void Run(){
	Collectdata();
	if (dcentro <= dextrem) {
		cout<<"LAS MEDIDAS SON ERR�NEAS, RECUERDA QUE EL DIAMENTRO EN EL CENTRO DEBE SER MAYOR QUE EL DE LOS EXTREMOS"<<endl<<endl;
		main();
	} else {
		Operations();
		Results();
		
	}
	
}
//=====================================================================================================


