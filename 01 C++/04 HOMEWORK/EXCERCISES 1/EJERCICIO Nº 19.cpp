/**
 * @file EJERCICIOS N� 19
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 19
	Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden 
	a los v�rtices de un triangulo, calcule su per�metro y �rea.

 * @version 1.0
 * @date 21.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define pi 3.1416

/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int a1,a2; 			// variables del 1� par ordenado
int b1,b2;   		// variables del 2� par ordenado
int c1,c2; 			// variables del 3� par ordenado
float d1, d2, d3; 	// variables de las distancias de punto a punto
float perimetro, semiperimetro;			// en base a la suma de los d's
float area;								// area del triangulo

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"SE CALCULAR� EL �REA Y EL PERIMETRO DE UN TRI�NGULO EN BASE A SUS COORDENADAS"<<endl;
	cout<<"INTRODUZCA LAS COORDENADAS DE CADA() PUNTO (primero la abscisa, luego la ordenada) "<<endl;
	cout<<"P1: "<<endl;cin>>a1;cin>>a2;
		cout<<"P1 = ("<< a1 << "," << a2<<")"<<endl;
	cout<<"P2: "<<endl;cin>>b1;cin>>b2;
		cout<<"P1 = ("<< b1 << "," << b2<<")"<<endl;
	cout<<"P3: "<<endl;cin>>c1;cin>>c2;
		cout<<"P1 = ("<< c1 << "," << c2<<")"<<endl;
    }
	
//=====================================================================================================

void Operations() {
	d1 = sqrt( pow((a1-b1),2) + pow((a2-b2),2) );
	d2 = sqrt( pow((a1-c1),2) + pow((a2-c2),2) );
	d3 = sqrt( pow((c1-b1),2) + pow((c2-b2),2) );
	
	perimetro = d1 + d2 + d3;
	semiperimetro = perimetro / 2;
	
	area = sqrt (semiperimetro*(semiperimetro - d1)*(semiperimetro - d2)*(semiperimetro - d3));
}
//=====================================================================================================

void Results(){
	cout<<"EL PERIMETRO DEL TRIANGULO ES: "<<perimetro<<endl;
	cout<<"EL AREA DEL TRIANGULO ES: "<< area << endl;
	
}
//=====================================================================================================

void Run(){
	Collectdata();
	Operations();
	Results();
	
	}
