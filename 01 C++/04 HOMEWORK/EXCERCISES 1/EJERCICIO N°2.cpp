	/*
HOMEWORK EXERCISE N�2

 * @Ejercicio N�2.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 02:
 		Elabore un algoritmo que dados como datos  de entrada cantidad de varones y cantidad de mujeres resolvamos que  
		Un maestro desea saber que porcentaje de hombres y que porcentaje de mujeres hay en un grupo de estudiantes. 

 * @version 1.0
 * @date 04.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
	//Description of the variable
	int  imputnumberofmen = 0;      //  Cantidad de varones
	int  imputnumberofwomen = 0;    //  Cantidad de mujeres
	int  total = 0;            //  Total de varones y mujeres
	float  womenpercentage = 0.0;   //  Porcentaje de mujeres calculado
	float  menpercentage = 0.0;     //  Porcentaje de varones calculado
	
/*******************************************************************************************************************************************
 *  												FUNCTIONS DECLARATION
 *******************************************************************************************************************************************/
 void Collectdata();
 void Operations();
 void Results();
 void Run();
 
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){

		Run();

	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTIONS DEFINITION
 *******************************************************************************************************************************************/
 
 void Collectdata(){
 	//Display phrase 1
	cout<<"=============INSERT DATA===============\r\n";
	cout<<"Escribe la cantidad de varones:";   //  Insertar el n�mero de varones presentes
	cin>>imputnumberofmen;                     //  Igualar el n�mero insertado con la variable varones
	cout<<"Escribe cantidad de mujeres:";      //  Insertar el n�mero de mujeres presentes
	cin>>imputnumberofwomen;                   //  Igualar el n�mero insertado con la variable mujeres
 }
 
 
 void Operations(){
 	//Operators
	total = imputnumberofmen+imputnumberofwomen;                             // Total de personas = varones + mujeres
	menpercentage = ((float)imputnumberofmen/(float)total)*(float)100;       // Porcentaje de varones = (varones/total)*100
	womenpercentage = ((float)imputnumberofwomen/(float)total)*(float)100;   // Porcentaje de mujeres = (mujeres/total)*100  
 }
 
 void Results(){
 	//Display phrase 2 with number
	cout<<"el porcentaja de hombres es :"<<menpercentage<<"%";
	cout<<"\nel porcentaje de mujeres es :"<<womenpercentage<<"%";
 	
 }
 
 void Run(){
 	Collectdata();
	Operations();
	Results();	
 }
 
 
 
