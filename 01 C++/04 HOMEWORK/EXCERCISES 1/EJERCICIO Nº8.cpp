/**
 * @file EJERCICIO N�8
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise N�8
 	Calcular el monto final, dados como datos el Capital Inicial, el  tipo de Inter�s, el numero de periodos por a�o
 	, y el numero de a�os de  la inversi�n. El c�lculo del capital final se basa en la formula del inter�s compuesto.
 										M = C(1+i/N)
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de a�os
	- Si un cliente deposita al Banco la cantidad de $10,000 a inter�s compuesto con una tasa del 8% anual.  
		�Cual ser� el monto que recaude despu�s de 9 a�os? 
	- �Cuanto debe cobrar el cliente dentro de 3 a�os si deposita  $ 100,000. al 9% anual  y capitaliz�ndose los intereses bimestralmente?   

 * @version 1.0
 * @date 12.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												CONSTANTS
 *******************************************************************************************************************************************/
 


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 
float capital, interes, years, number;    // variables que se utilizar�n para las operaciones
int tipe;									// variable que se usar� para el switch
float monto;								// variable que representar� el monto obtenido

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION																   *
 *******************************************************************************************************************************************/
 
void Collectdata();					// Funcion que recolectar� datos
void Operations();					// Funci�n que realizar� las operaciones matem�ticas	
void Results();						// Funcion que imprimir� los resultados obtenidos
void Run();							// Funci�n que contendr� a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	cout<<"SE VA A CALCULAR EL MONTO"<<endl;
		
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"Ingrese el capital inicial depositado: "<<endl;
	cin>>capital;
	cout<<"Ingrese cada cuanto se va capitalizar: "<<endl;
	cout<<"MENSUAL= '1', BIMESTRAL = '2', TRIMESTRAL = '3', SEMESTRAL = '4', ANUAL = '5' "<<endl;
	cin>>tipe;
	cout<<"Ingrese el inter�s anual: "<<endl;
	cin>>interes;
	cout<<"Ingrese el n�mero de a�os en el que se retirar�: "<<endl;
	cin>>years;
	
}

//=====================================================================================================

void Operations(){
	
			switch(tipe){
		case 1 : number=12; break;
		case 2 : number=6; break;
		case 3 : number=4; break;
		case 4 : number=2; break;
		case 5 : number=1; break;
		default: number=1; break;
	}
	 
	 monto= capital * pow(1+(interes/(100*number)),years*number);
	
}
//=====================================================================================================

void Results(){
	
	cout<<"El monto a recoger es de: "<< monto <<endl;
	
}

//=====================================================================================================
void Run(){
	Collectdata();
	Operations();
	Results();
}



