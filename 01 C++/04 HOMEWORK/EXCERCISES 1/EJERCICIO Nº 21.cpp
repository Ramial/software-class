/**
 * @file EJERCICIOS N� 21
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 21
	Calcular la velocidad de un auto en Km/h, ingresando la 
	distancia recorrida en metros y el tiempo en minutos.

 * @version 1.0
 * @date 21.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float metros, minutos;		//datos a ingresar
float kilometers, hours;	// datos a convertir
float velocidad;			// variable que representa la velocidad

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/


void Collectdata(){	
	cout<<"ESTE PROGRAMA TRANSFORMAR� LA VELOCIDAD DE METROS POR SEGUNDO A KILOMETROS POR SEGUNDO"<<endl;
	cout<<"====================================INSERT DATA=============================================="<<endl;
	cout<<"INGRESA LA DISTANCIA RECORRIDA EN METROS: "; cin>>metros;
	cout<<"INGRESA EL TIEMPO EN MINUTOS: "; cin >> minutos;
}

void calculate(){
	kilometers = metros / 1000;
	hours = minutos / 60;
	
	velocidad = kilometers / hours ;
}

void showresults(){
	cout<<"================================RESULTS================================================="<<endl;
	cout<<"LA VELOCIDAD EN KILOMETROS POR HORA ES: "<< velocidad <<" km/h"<<endl;
}

void Run(){
	Collectdata();
	calculate();
	showresults();
}
