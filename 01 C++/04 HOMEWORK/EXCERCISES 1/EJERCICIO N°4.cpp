/*
HOMEWORK EXERCISE N�4

 * @Ejercicio N�4.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 03:
 		4.- Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico. Dise�ar un algoritmo para realizar 
		las siguientes conversiones:
		a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
		b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada.


 * @version 1.0
 * @date 08.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  										FUNCTION DECLARATIONS
 *******************************************************************************************************************************************/
void Run();                 // Funci�n que contendr� a las dem�s funciones
void collectData();         // Funci�n que se encargar� de recolectar los datos a usar  
void calculate();           // Funci�n que usar� los datos para realizar operaciones matem�ticas
void showresults();         // Funci�n que mostrar� los resultados de las operaciones matem�ticas 

/*******************************************************************************************************************************************
 *  										 GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 
 float temperatureC = 0;     // Temperatura en Celsius
 float waterI = 0;           // Cantidad de agua en pulgadas 
 	
 float watermm = 0.0;        // Cantidad de agua en milimetros 
 float temperatureF = 0.0;   // Temperatura en Farenheit
 
/*******************************************************************************************************************************************
 *  										         MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  										FUNCTION DEFINITIONS
 *******************************************************************************************************************************************/
 void collectData(){
 	// Display phrase
	cout<<"=====================INSERT DATA============================";
	cout<<"\r\nEscribe la temperatura en C�:";           // Insertar la temperatura en Celcius
	cin>>temperatureC;                                   // Igualar el dato insertado con la variable 
	cout<<"Escribe la cantidad de agua en pulgadas:";    // Insertar la cantidad de agua en pulgadas
	cin>>waterI;	                                     // Igualar el dato insertado con la variable
 }
 
 void calculate(){
 	// Operators
	temperatureF= ((9 * temperatureC)/5) + 32;           // Formula Celcius a Farenheit "F=9/5 �C+32"
	watermm = waterI * 2.25;	                         // Formula Pulgadas a mil�metros 25.5 mm = 1pulgada
 }
 
 void results(){
 	// Display Phrase with number
	cout<<"===================RESULTS===========================";
	cout<<"\r\nLa temperatura en F� es:"<<temperatureF;              // Impirmir el resultado temperatura
	cout<<"\r\nLa cantidad de agua en milimetros es:"<<watermm;	     // Imprimir el resultado agua 
 }
 
 void Run(){
 	collectData(); 
	calculate();
 	results();	
 }
