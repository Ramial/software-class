/**
 * @EJERCICIO N� 7.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excersice N� 7
 		7.- Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, mediante la siguiente f�rmula:
		F = G*masa1*masa2 / distancia2
		Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 cm3/g.seg2

 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												CONSTANTS
 *******************************************************************************************************************************************/
const double G = 6.673 * pow(10,-8);	//constante de gravitaci�n universal

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double masa1,masa2,distancia;			// variables a usar 
double fuerza = 0;						// variable para el resultado

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata();			//funci�n que recolecta la informaci�n
void Operations();			//Funci�n donde se realizan las operaciones matem�ticas
void Results(); 			//Funci�n donde se imprimir�n los resultados
void Run();					//Funci�n que contendr� al resto

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"ESTE PROGRAMA CALCULAR� LAS FUERZAS DE ATRACCI�N DE 2 MASAS, SEPARADAS A UNA DISTANCIA"<<endl;
	cout<<"==================INSERT DATA====================="<<endl;
	cout<<"Ingrese la masa del primer cuerpo: ";
	cin>>masa1;
	cout<<"Ingrese la masa del segundo cuerpo: "; 
	cin>>masa2;
	cout<<"Ingrese la distancia que los separa: " ;
	cin>>distancia;
		
}
//=====================================================================================================

void Operations(){
	fuerza = (masa1*masa2*G)/pow(distancia,2);		// Formula para hallar la atracci�n entre 2 cuerpos
}
//=====================================================================================================

void Results(){
	cout<<"La fuerza de atracci�n entre las dos masas es: "<< fuerza ;
}

//=====================================================================================================

void Run(){
	Collectdata();
	Operations();
	Results();
}
