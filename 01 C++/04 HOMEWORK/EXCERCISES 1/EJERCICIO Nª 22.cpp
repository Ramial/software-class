/**
 * @file EJERCICIOS N� 22
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 22	
	Dado un tiempo expresado en HH:MM y otro tiempo en MM: SS, dise�e un programa que 
	calcule la suma de los tiempos y lo exprese en HH:MM:SS.

 * @version 1.0
 * @date 22.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int horas, minutos1;
int minutos2, segundos;
int tiempo;
int horasf, minutosf, segundosf;
char c;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	Collectdata();
	Operations();
	Results();
}

void Collectdata(){	
	cout<<"EL SIGUIENTE PROGRAMA SUMAR� DOS TIPOS DE TIEMPOS HORAS:MINUTOS Y MINUTOS:HORAS"<<endl;
	cout<<"Formato a introducir hh : mm (los dos punto son necesarios)"<<endl;
	cout<<"=============================INSERT DATA================================="<<endl;
	cout<<"INGRESE EL TIEMPO 'HORA:MINUTOS': "; cin>> horas >> c >>minutos1;
	cout<<"INGRESE EL TIEMPO 'MINUTOS:SEGUNDOS': ";cin >> minutos2 >> c >> segundos;
	cout<<"=============================DATA CONFIRMATION==========================="<<endl;
	cout<<"LO TIEMPOS A SUMAR SON: "<<endl;
	cout<< horas <<":"<< minutos1<<":"<<"00"<<endl;
	cout<< "00:"<<minutos2<<":" << segundos <<endl;
}
void Operations(){
 	horas = horas * 60 * 60;
 	minutos1 = minutos1 *60;
 	minutos2 = minutos2 *60;
 	
 	tiempo = minutos1 + minutos2 + horas + segundos;
 	
 	horasf = tiempo /3600;
 	tiempo = tiempo % 3600;
 	minutosf = tiempo / 60;
 	tiempo = tiempo % 60;
 	segundosf = tiempo;
 	
}

void Results(){
	cout<<"===============================RESULTS===================================="<<endl;
	cout<<"EL TIEMPO SUMADO DA: "<<endl;
	cout<< horasf << ":" << minutosf << ":" << segundosf;
}

