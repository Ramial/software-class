/**
 * @file EJERCICIO N�9
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise N�9
 	�Cu�l es el capital que debe colocarse a inter�s  compuesto del 8%  anual para que 
	 despu�s de 20 a�os produzca un monto de $ 500,000. ? 

 * @version 1.0
 * @date 15.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												CONSTANTS
 *******************************************************************************************************************************************/
 


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 
double monto, interes, years;				// variables a utilizar en las operaciones matem�ticas
double capital;								// variable que representa el resultado obtenido

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION																   *
 *******************************************************************************************************************************************/
 
void Collectdata();							// funcion que recolectar� informaci�n
void Operations();							// funcion que realizar� las operaciones matem�ticas
void Results();								// funci�n que imprimir� los resultados
void Run();									// funcion que contendr� a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
		
	cout<<"SE VA A CALCULAR EL CAPITAL A DEPOSITAR PARA LLEGAR A DICHO MONTO"<<endl;
		
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	
	cout<<"Ingrese el monto que se desea obtener: "<<endl;
	cin>>monto;
	cout<<"Ingrese el inter�s anual: "<<endl;
	cin>>interes;
	cout<<"Ingrese el n�mero de a�os en el que se retirar�: "<<endl;
	cin>>years;
	
}

//=====================================================================================================

void Operations(){
	 
	capital = monto / pow(1+(interes/100),years);
	
}
//=====================================================================================================

void Results(){
	
	cout<<"El capital que se debe depositar es: "<< capital <<endl;
	
}

//=====================================================================================================
void Run(){
	Collectdata();
	Operations();
	Results();
}
