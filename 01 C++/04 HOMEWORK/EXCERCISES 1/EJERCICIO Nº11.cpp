/**
 * @file EJERCICIO 11
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIO N�11
 	11.- Hacer un algoritmo para ingresar una medida en metros, y que imprima esa medida expresada en cent�metros,
	  pulgadas, pies y yardas. Los factores de conversi�n son los siguientes:
1 yarda = 3 pies
1 pie = 12 pulgadas
1 metro = 100 cent�metros
1 pulgada = 2.54 cent�metros

 * @version 1.0
 * @date 19�.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float centimeters, pies, yardas, metros, pulgadas;	//variables a usar

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void collectdata();				// Recolecta la informacion
void operations();				// Realiza las operaciones
void results();					// Imprime resultados
void run();						// Contiene las dem�s funciones
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void collectdata(){
	cout<< "el siguiente programa convertira los centimetros ingresados en: "<<endl;
	cout<< "pies, yardas, metros y pulgadas"<<endl;
	cout<< "Ingrese los centimetros a convertir"<<endl;
	cin>>centimeters;
}


//=====================================================================================================

void operations(){
	metros = centimeters/100;
	pulgadas = centimeters/(2.54);
	pies = pulgadas / 12;
	yardas = pies / 3;
	
}
//=====================================================================================================

void results(){
	cout<<"En metros es: "<< metros <<endl;
	cout<<"En pulgadas es: "<< pulgadas <<endl;
	cout<<"En pies es: "<< pies <<endl;
	cout<<"En yardas es: "<< yardas <<endl;
}

//======================================================================================================

void run(){
	collectdata();
	operations();
	results();
}
