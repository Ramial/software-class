/**
 * @file EJERCICIOS N� 14
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 14
 14. Modifique el programa anterior para que, suponiendo que las medidas de entrada son dadas en cent�metros, 
 		el resultado lo muestre en: litros, cent�metros c�bicos y metros c�bicos.
  
  Recuerde que 1 litro es equivalente a un dec�metro c�bico. Indique siempre la unidad de medida empleada.
 
 * @version 1.0
 * @date 19.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define pi  3.1416;

/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float dcentro, dextrem, altura, largo, volumen;  // variables a usar en el problema
float litro, mcubicos;							 // variables para la conversi�n 
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void Operations();			// Realiza las operaciones matem�ticas
void Results();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"ESTE PROGRAMA HALLAR� EL VOLUMEN DE UN TONEL"<<endl;
	cout<<"INGRESE LA LONGITUD DEL TONEL EN CENTIMETROS: "<<endl;
	cin>>largo;
	cout<<"INGRESE EL DIAMETRO EN EL CENTRO DEL TONEL EN CENTIMETROS"<<endl;
	cin>>dcentro;
	cout<<"INGRESE EL DIAMETRO EN LOS EXTREMOS DEL TONEL EN CENTIMETROS"<<endl;
	cin>>dextrem;
}
//=====================================================================================================

void Operations(){
		
	altura = (dextrem/2) + (2/3)*(dcentro/2)+(2*3)*(dextrem/2);
	volumen = 3.1416 * largo * pow (altura,2);
	
	litro = volumen / 1000;
	mcubicos = litro / 1000;
}
//=====================================================================================================

void Results(){
	cout<<"EL VOLUMEN DE DICHO TONEL ES: "<< volumen << " CENTIMETROS C�BICOS"<<endl;
	cout<<"EL VOLUMEN DE DICHO TONEL ES: "<< litro << " LITROS"<<endl;
	cout<<"EL VOLUMEN DE DICHO TONEL ES: "<< mcubicos << " METROS C�BICOS"<<endl;
	
}
//=====================================================================================================

void Run(){
	Collectdata();
	if (dcentro <= dextrem) {
		cout<<"LAS MEDIDAS SON ERR�NEAS, RECUERDA QUE EL DIAMENTRO EN EL CENTRO DEBE SER MAYOR QUE EL DE LOS EXTREMOS"<<endl<<endl;
		main();
	} else {
		Operations();
		Results();
		
	}
	
}
//=====================================================================================================


