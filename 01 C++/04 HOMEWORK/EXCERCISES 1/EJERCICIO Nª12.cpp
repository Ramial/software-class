/**
 * @file EJERCICIO N� 12
 * @author Ramirez Alanya Gianfrnco (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIO N� 12
 12.- Escriba un programa para calcular el tiempo transcurrido, en minutos, necesario para hacer un viaje. 
 La ecuaci�n es tiempo transcurrido = distancia total/velocidad promedio. Suponga que la distancia est� en kil�metros 
  	y la velocidad en kil�metros/hora. 
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float distancia, speed;			// variables a pedir
float time, minutes;      		// variables para calcular lo pedido		

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata();			// recolecta informacion
void Operations();			// realiza operaciones matematicas
void Results();				// Imprime resultados
void Run();					//contiene a funciones 
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"Este programa calcular� el tiempo que toma un recorrido en minutos"<<endl;
	cout<<"Ingrese la velocidad promedio en kilometros/hora"<<endl;
	cin>>speed;
	cout<<"Ingrese la distancia total a recorrer"<<endl;
	cin>>distancia;
	
}
//=====================================================================================================

void Operations(){
	time = distancia / speed;
	minutes = time * 60;
	
}
//=====================================================================================================

void Results(){
	cout<<"El tiempo en minutos que tomar� el recorrido es: "<< minutes <<endl;

}
//=====================================================================================================

void Run(){
	Collectdata();
	if(distancia <= 0 || speed <= 0){
		cout<<"INTRODUZCA N�MEROS POSITIVOS"<<endl<<endl;
		main();	
	}
	Operations();
	Results();
}



