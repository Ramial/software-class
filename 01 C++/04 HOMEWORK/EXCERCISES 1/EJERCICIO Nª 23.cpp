/**
 * @file EJERCICIOS N� 23
 * @author Gianfranco Ramirez Alanya (gianfranco.ramireza@gmail.com)
 * @brief EJERCICIOS N� 23
		En todo triangulo se cumple que cada lado es proporcional al seno del �ngulo opuesto. 
		Esta ley se llama la ley de los senos Matem�ticamente.
		a/senA=b/senB=c/senC
		
		si se conocen los 3 angulo y solo un lado.... calula los dem�s lados

 * @version 1.0
 * @date 21.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 22/7

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float lado1;				// variable del lado que se conoce
float anguloopuesto;		// variable del lado opuesto que se conoce
float angulo1, angulo2;		// variables de los otros angulos conocidos
float lado2, lado3;			// lados que se van a hallar
float csobreseno;			// variable que iguala a c/senc de la formula


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Collectdata(); 		//recolecta informacion
void calculate();			// Realiza las operaciones matem�ticas
void showresults();				// Imprime los resultados
void Run();					// Contiene a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	Collectdata();
	calculate();
	showresults();
}

void Collectdata(){	
	cout<<"ESTE PROGRAMA HALLAR� LOS LADOS FALTANTES DE UN TRIANGULO CON UN LADO Y LOS ANGULOS"<<endl;
	cout<<"=====================================INSERT DATA======================================"<<endl;
	cout<<"INGRESE EL LADO QUE SE CONOCE: ";cin>>lado1;
	cout<<"INGRESE SU ANGULO OPUESTO: ";cin>>anguloopuesto;
	cout<<"INGRESE LOS ANGULOS RESTANTES: "; cin >> angulo1 >> angulo2;
}

void calculate(){
	csobreseno = lado1/sin(anguloopuesto*PI/180);
	
	lado2 = csobreseno * sin(angulo1 * PI / 180);
	lado3 = csobreseno * sin(angulo2 * PI / 180);
}

void showresults(){
	cout<<"============================RESULTS==========================="<<endl;
	cout<<"LOS LADOS RESTANTES SON: "<<endl<<lado2<<endl<<lado3;

}
