/**
 * @file EJERCICIO N�10
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief EXCERSICE N�10
 Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. Al morir dej� el siguiente legado:
  A Jos� le dej� 4/3 de lo que le dej� a Carlos. A Carlos le dej� 1/3 de su fortuna. A Marta le dejo 
  la mitad de lo que le dej� a Jos�. Preparar un algoritmo para darle la suma a repartir e imprima cuanto 
  le toc� a cada uno
  
 * @version 1.0
 * @date 15.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												CONSTANTS
 *******************************************************************************************************************************************/
 


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int monto;						// variable que se pedira
float carlos, jose, marta; 		// variable que representar� qu� le toc� a cada uno

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION																   *
 *******************************************************************************************************************************************/
 
void Collectdata();					// Funcion que recolectar� datos
void Operations();					// Funci�n que realizar� las operaciones matem�ticas	
void Results();						// Funcion que imprimir� los resultados obtenidos
void Run();							// Funci�n que contendr� a las dem�s funciones

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	

	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Collectdata(){
	cout<<"SE VA A CALCULAR EL MONTO QUE LE CORRESPODER� A CADA HIJO"<<endl;
	cout<<"INGRESE EL MONTO A REPARTIR: "<<endl;
	cin>>monto;
}

//=====================================================================================================

void Operations(){
	carlos = monto / 3;
	jose = 4 * carlos / 3;
	marta = jose / 2;
}
//=====================================================================================================

void Results(){
	cout<<"CARLOS RECIBI�: "<<carlos<<endl;
	cout<<"JOS� RECIBI�: "<< jose <<endl;
	cout<<"MARTA RECIBI�: "<<marta<<endl;
	
}

//=====================================================================================================
void Run(){
	Collectdata();
	Operations();
	Results();

}

