/*
HOMEWORK EXERCISE N�5

 * @Ejercicio N�5.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 05:
 		El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo, del porcentaje de la ganancia del vendedor 
		y de los impuestos locales o estatales aplicables (sobre el precio de venta). Suponer una ganancia del vendedor del 12% en todas las 
		unidades y un impuesto del 6% y dise�ar un algoritmo para leer el costo total del autom�vil e imprimir el costo para el consumidor.

 * @version 1.0
 * @date 08.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  										FUNCTION DECLARATIONS
 *******************************************************************************************************************************************/
void Run();                 // Funci�n que contendr� a las dem�s funciones
void collectData();         // Funci�n que se encargar� de recolectar los datos a usar  
void calculate();           // Funci�n que usar� los datos para realizar operaciones matem�ticas
void showresults();         // Funci�n que mostrar� los resultados de las operaciones matem�ticas 

/*******************************************************************************************************************************************
 *  										FUNCTION DECLARATIONS
 *******************************************************************************************************************************************/
 
 // Global variables
 float carcost = 0.0;		// costo de compra del carro
 float carprice	 = 0.0;		// precio de venta del cauto
 	
 
/*******************************************************************************************************************************************
 *  										         MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  										FUNCTION DEFINITIONS
 *******************************************************************************************************************************************/
 void collectData(){
 	// Display phrase
	cout<<"=====================INSERT DATA============================";
	cout<<"\r\nEscribe el costo total del auto:";
	cin>>carcost;	
 }
 
 void calculate(){
 	// Operators
	carprice= (carcost*1.12)*1.06;	
 }
 
 void results(){
 	// Display Phrase with number
	cout<<"===================RESULTS===========================";
	cout<<"\r\nEl precio de venta del auto es:"<<carprice;	
 }
 
 void Run(){
 	collectData(); 
	calculate();
 	results();	
 }
