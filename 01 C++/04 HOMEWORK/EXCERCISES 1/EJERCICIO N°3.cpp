/*
HOMEWORK EXERCISE N�3

 * @Ejercicio N�3.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 03:
 		3.- Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, necesitamos un algoritmo que lea el n�mero de desaprobados, 
		 aprobados, notables y sobresalientes de una asignatura, y nos devuelva:
		a. El tanto por ciento de alumnos que han superado la asignatura.
		b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.
 

 * @version 1.0
 * @date 05.12.2021
 * 
*/

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
	//Description of the variable
	int   imputdesaprobados = 0;             //  Cantidad de desaprobados
	int   imputaprobados = 0;                //  Cantidad de aprobados
	int   imputnotables = 0;                 //  Cantidad de notables
	int   imputsobresalientes = 0;           //  Cantidad de sobresalientes
	int   total =0;                          //  Cantidad total de personas calificadas
	int   passedthecourse = 0;               //  Cantidad de estudiantes que pasaron el curso   
	float   passedthecoursepercentage = 0.0;   //  Porcentaje de estudiantes que pasaron el curso 
	float   aprobadospercentage = 0.0;         //  Porcentaje de estudiantes aprobados
	float   desaprobadospercentage = 0.0;      //  Porcentaje de estudiantes desaprobados
	float   sobresalientespercentage = 0.0;    //  Porcentaje de estudiantes sobresalientes
	float   notablespercentage = 0.0;          //  Porcentaje de estudiantes notables




/*******************************************************************************************************************************************
 *  												FUNCTIONS DECLARATION
 *******************************************************************************************************************************************/
 void Collectdata();
 void Operations();
 void Results();
 void Run();
 
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){

	Run();
	
	return 0;
}


void Collectdata(){
			
	//Display phrase 1
	cout<<"=============INSERT DATA===============\r\n";
	cout<<"Escribe la cantidad de desaprobados:";   //  Insertar el n�mero de desaprobados
	cin>>imputdesaprobados;                         //  Igualar el n�mero insertado con la variable imputdesaprobados 
	cout<<"Escribe la cantidad de aprobados:";      //  Insertar el n�mero de aprobados
	cin>>imputaprobados;                            //  Igualar el n�mero insertado con la variable imputaprobados
	cout<<"Escribe la cantidad de notables:";        //  Insertar el n�mero de notables   
	cin>>imputnotables;                              //  Igualar el n�mero insertado con la variable imputnotables
	cout<<"Escribe la cantidad de sobresalientes:";  //  Insertar el n�mero de sobresalientes
	cin>>imputsobresalientes;                        //  Igualar el n�mero insertado con la variable imputnotables
}

void Operations(){
		//Operators
	total = imputaprobados + imputdesaprobados + imputnotables + imputsobresalientes;       // Total de personas calificadas
	passedthecourse = (imputaprobados + imputnotables + imputsobresalientes);               // Total de personas que pasaron el curso
	passedthecoursepercentage = ((float)passedthecourse/(float)total)*(float)100;           // Porcentaje de personas que pasaron el curso = (personas que pasaron/total)*100
 	desaprobadospercentage = ((float)imputdesaprobados/(float)total)*(float)100;            // Porcentaje de desaprobados = (desaprobados/total)*100
	aprobadospercentage = ((float)imputaprobados/(float)total)*(float)100;                  // Porcentaje de aprobados = (aprobados/total)*100
	notablespercentage = ((float)imputnotables/(float)total)*(float)100;                    // Porcentaje de notables = (notables/total)*100
	sobresalientespercentage = ((float)imputsobresalientes/(float)total)*(float)100;        // Porcentaje de sobresalientes = (sobresalientes/total)*100  
}

void Results(){
		//Display phrase 2 with number
	cout<<"el porcentaje de personas que pasaron el curso es :"<<passedthecoursepercentage<<"%";
	cout<<"\nel porcentaje de desaprobados es :"<<desaprobadospercentage<<"%";
	cout<<"\nel porcentaja de aprobados es :"<<aprobadospercentage<<"%";
	cout<<"\nel porcentaje de notables es :"<<notablespercentage<<"%";
	cout<<"\nel porcentaje de sobresalientes es :"<<sobresalientespercentage<<"%";	
}

void Run(){
	Collectdata();
	Operations();
	Results();
}
