/**
 * @Ejercicio N�1.cpp
 * @author Gianfranco Ramirez (gianfranco.ramireza@gmail.com)
 * @brief Excercise 01:
 		Elabore un algoritmo que dados como datos  de
		entrada el radio y la altura de un cilindro calcular, el rea lateral y el volumen del cilindro.
		A = pi^2radio*altura	V =radio^2pi*altura
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416		// Constante Pi 

/*******************************************************************************************************************************************
 *  										FUNCTION DECLARATIONS
 *******************************************************************************************************************************************/
void Run();                 // Funci�n que contendr� a las dem�s funciones
void collectData();         // Funci�n que se encargar� de recolectar los datos a usar  
void calculate();           // Funci�n que usar� los datos para realizar operaciones matem�ticas
void showresults();         // Funci�n que mostrar� los resultados de las operaciones matem�ticas 

/*******************************************************************************************************************************************
 *  										GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float  imputheight = 0.0;             // Entrada de altura del cilindro
float  imputradio = 0.0;              // Rntrada de radio del cilindro 
float  lateralareacircle = 0.0;       // �rea Lateral del cilindro calculada
float  volumecircle = 0.0;            // Volume del cilindro calculado

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
 Run();
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	collectData();
	calculate();
	showresults();
}

void collectData(){	
	cout<<"================================INSERT DATA===================================";
	cout<<"\r\nEscribe el valor de la altura:";
	cin>>imputheight;
	cout<<"Escribe el valor del radio:";
	cin>>imputradio;
}

void calculate(){

	lateralareacircle = imputheight*imputradio*PI*2;         //Area lateral del cilindro = Altura * Radio * 2 * pi
	volumecircle = PI*imputradio*imputradio*imputheight;     //Volume del cilindro = pi * radio�2 * Altura
}

void showresults(){
	cout<<"el resultado de su area lateral es :"<<lateralareacircle;
	cout<<"\nel resultado de su volumen es :"<<volumecircle;
}
